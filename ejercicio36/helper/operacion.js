const fs = require("fs");

function rangoAle(limiteInferior, limiteSuperior) {
  return (
    Math.floor(Math.random() * (limiteSuperior - limiteInferior + 1)) +
    limiteInferior
  );
}

for (var i = 0; i < 10; i++) {}
let numberq = rangoAle(100, 700000);

const crearArchivo = (number = numberq) => {
  try {
    console.log("valor inicial", number);
    let cadena = "";
    while (number > 1) {
      if (number % 2 === 0) {
        number = number / 2;
        cadena = number;
      } else {
        number = 3 * number + 1;
        cadena = number;
      }
      console.log(cadena, "000");
    }
    fs.writeFile(
      `salida-operaciones/Tabla-suma-${number}.txt`,
      cadena,
      (err) => {
        if (err) throw err;
        console.log(`Tabla-suma-${number}.txt creado`);
      }
    );
  } catch (error) {}
};

module.exports = {
  generarArchivos: crearArchivo,
};
