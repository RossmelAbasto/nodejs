const fs = require('fs')
const DBRoute = './data/DB.json'

class DataPersona {
  constructor() {
    this.data = []
    this.leer = fs.readFileSync
    this.escribir = fs.writeFileSync
    this.cargardatos()
  }
  cargardatos = () => {
    if (!fs.existsSync(DBRoute) || this.data.length !== 0) {
      return null
    } else {
      let rawdata = this.leer(DBRoute)
      this.data = JSON.parse(rawdata)
    }
  }
  guardar = () => {
    let data = JSON.stringify(this.data, null, 2);
    this.escribir(DBRoute, data)
  }
  getPersonas = () => {
    return this.data
  }
  addPersona = (persona) => {
    this.data.push(persona)
    this.guardar()
  }
  editarPersona = (persona, index) => {
    this.data[index].nombre = persona.nombre
    this.data[index].apellido = persona.apellido
    this.data[index].ci = persona.ci
    this.data[index].direccion = persona.direccion
    this.data[index].sexo = persona.sexo
    this.guardar()
  }
  eliminarPersona = (id) => {
    this.data.splice(id, 1)
    this.guardar()
  }
}

module.exports = DataPersona
