const {Router} = require('express')
const {
  personaGet,
  personaPost,
  personaPut,
  personaDelete
} = require('../controllers/personas')


const route = Router()

route.get('/', personaGet)
route.post('/', personaPost)
route.put('/:id', personaPut)
route.delete('/:id', personaDelete)


module.exports = route