const { response , request } = require('express')
const DataPersona = require('../data/data-person')
const Persona = require('../models/Persona')

const dataPerson = new DataPersona()

const personaGet = (req, res = response) => {

  let datos = dataPerson.getPersonas()

  res.json({
    datos
  })
}

const personaPost = (req = request, res = response) => {
  const { nombre, apellidos, ci, direccion, sexo } = req.body;

  let personaNueva = new Persona(nombre, apellidos, ci, direccion, sexo)

  dataPerson.addPersona(personaNueva)

  res.json({
    personaNueva,
    msg: 'Person Added Sucessfully'
  })
}

const personaPut = (req = request, res = response) => {
  const {id} = req.params
  const { nombre, apellidos, ci, direccion, sexo } = req.body;

  const index = dataPerson.data.findIndex(object => object.id === id)

  if (index !== -1) {
    dataPerson.editarPersona({ nombre, apellidos, ci, direccion, sexo }, index)
    res.json({
      nombre,
      apellidos,
      ci,
      direccion,
      sexo,
      msg: 'Person Was Modified Sucessfully'
    })
  } else {
    res.json({
      msg: 'Person Not Found'
    })
  }
}

const personaDelete = (req = request, res = response) => {
  const {id} = req.params
  const index = dataPerson.data.findIndex(object => object.id === id)
  if (index !== -1) {
    dataPerson.eliminarPersona(index)
    res.json({
      msg: 'The Person Was Sucessfully Removed'
    })
  } else {
    res.json({
      msg: 'Person Not Found'
    })
  }
}

module.exports = {
  personaGet,
  personaPost,
  personaPut,
  personaDelete
}