let fecha = new Date();

let diaSemana = [
  "Domingo",
  "Lunes",
  "Martes",
  "Miercoles",
  "Jueves",
  "Viernes",
  "Sabado",
];
let diaSemana1 = diaSemana[fecha.getDay()];

let hora = new Date();
hora = hora.toLocaleTimeString();

let mesAnyo = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo ",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
];
let mesAnyo1 = mesAnyo[fecha.getMonth()];

date_2 = ` ${fecha.getDate()} / ${
  fecha.getMonth() + 1
} / ${fecha.getFullYear()}`;

var ampm;
if (hora >= 12) {
  hora = hora - 12;
  ampm = "PM";
} else {
  ampm = "AM";
}
if (hora == 0) {
  hora == 12;
}

class Hora {
  constructor() {
    const datosHora = {
      day: diaSemana1,
      date_1: `${fecha.getDate()} de ${mesAnyo1} `,
      Year: fecha.getFullYear(),
      date_2: date_2,
      hour: hora,
      prefix: ampm,
    };
    this.datosHora = datosHora;
  }
}

module.exports = Hora;
