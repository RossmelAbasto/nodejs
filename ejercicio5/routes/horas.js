const {Router} = require('express')

const {
  obtenerHora
} = require('../controllers/horas')

const route = Router()
route.get('/', obtenerHora)

module.exports = route