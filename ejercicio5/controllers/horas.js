const { response, request } = require("express");
const Hora = require("../data/data-horas");

const dataHora = new Hora();

const obtenerHora = (req, res = response) => {
  let datos = dataHora.datosHora;
  res.json(datos);
};

module.exports = { obtenerHora };
