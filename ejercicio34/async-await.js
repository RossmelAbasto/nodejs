const clientes = [
  {
    id: 1,
    name: 'Leonor'
  },

  {
    id: 2,
    name: 'Jacinto'
  },

  {
    id: 3,
    name: 'Waldo'
  }
];


const payments = [
  {
    id: 1,
    payment: 1000
  },

  {
    id: 2,
    payment: 1800
  }

];

const id = 1;

const getCliente = (id) => {
  return new Promise((resolve, reject) => {
    const cliente = clientes.find(e => e.id === id);
    if(cliente){
      resolve(cliente);
    } else {
      reject(`No existe el cliente con el id ${id}`);
    }
  });
};

const getPago = (id) => {
  return new Promise((resolve, reject) => {
    const payment = payments.find(s => s.id === id);
    if(payment){
      resolve(payment);
    } else {
      reject(`No existe el pago del id ${id}`);
    }
  });  
}

const getInfoCliente = async (id) => {
  try {
    const cliente = await getCliente(id);
    const payment = await getPago(id);
    return `${cliente.name} payment: ${payment['payment']} Bs`;
  }
  catch(ex){
    throw ex;
  }
};
getInfoCliente(id)
  .then(message => console.log(message))
  .catch(err => console.log(err));

getInfoCliente(2)
  .then(message => console.log(message))
  .catch(err => console.log(err));

getInfoCliente(3)
  .then(message => console.log(message))
  .catch(err => console.log(err));
  
  