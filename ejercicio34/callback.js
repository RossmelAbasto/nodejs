const clientes = [
  { id: 1, nombre: "Leonor" },
  { id: 2, nombre: "Jacinto" },
  { id: 3, nombre: "Waldo" },
];
const pagos = [
  { id: 1, pago: 1000, moneda: "Bs" },
  { id: 2, pago: 1800, moneda: "Bs" },
];


const getClienteYPagoWithCallback = (id, callback) => {
  const cliente = clientes.find((c) => c.id === id);
  const pago = pagos.find((p) => p.id === id);
  callback(cliente, pago);
};

for (let i = 1; i <=3; i++) {
  getClienteYPagoWithCallback(i, (cliente, pago) => {
    console.log("Cliente:", cliente, "Pago:", pago);
  });
}