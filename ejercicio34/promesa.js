const clientes = [
  {
    id: 1,
    nombre: 'Leonor'
  },

  {
    id: 2,
    nombre: 'Jacinto'
  },

  {
    id: 3,
    nombre: 'Waldo'
  }
];


const pagos = [
  {
    id: 1,
    pago: 1000
  },

  {
    id: 2,
    salario: 1800
  }

];

const id = 1;

const getCliente = (id) => {
  return new Promise((resolve, reject) => {
    const cliente = clientes.find(e => e.id === id);
    if(cliente){
      resolve(cliente);
    } else {
      reject(`No existe el cliente con el id ${id}`);
    }
  });
};

const getPago = (id) => {
  return new Promise((resolve, reject) => {
    const pago = pagos.find(s => s.id === id);
    if(pago){
      resolve(pago);
    } else {
      reject(`No existe el pago con el id ${id}`);
    }
  });  
}

getCliente(id)
.then(cliente => console.log(cliente))
.catch(error => {
  console.log(error);
});

getPago(id)
.then(pago => console.log(pago))
.catch(error => {
  console.log(error);
});

let nombre;

getCliente(id)
.then(cliente => {
  nombre = cliente.nombre;
  return getPago(id);
})

.then(pago => {
  console.log(nombre, 'pago:', pago['pago'], 'Bs');
})

.catch(error => {
  console.log(error);
});



