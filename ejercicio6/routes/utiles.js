const {Router} = require('express')
const {
  utilGet,
  utilPost,
  utilPut,
  utilDelete
} = require('../controllers/utilesescolares')


const route = Router()

route.get('/', utilGet)
route.post('/', utilPost)
route.put('/:id', utilPut)
route.delete('/:id', utilDelete)

module.exports = route