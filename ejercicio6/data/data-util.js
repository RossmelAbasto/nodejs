const fs = require("fs");
const DBRoute = "./data/DB.json";

class DataUtil {
  constructor() {
    this.data = [];
    this.leer = fs.readFileSync;
    this.escribir = fs.writeFileSync;
    this.cargardatos();
  }

  cargardatos = () => {
    if (!fs.existsSync(DBRoute) || this.data.length !== 0) {
      return null;
    } else {
      let rawdata = this.leer(DBRoute);
      this.data = JSON.parse(rawdata);
    }
  };

  guardar = () => {
    let data = JSON.stringify(this.data, null, 2);
    this.escribir(DBRoute, data);
  };

  getUtiles = () => {
    return this.data;
  };

  addUtil = (util) => {
    this.data.push(util);
    this.guardar();
  };

  editarUtil = (util, index) => {
    this.data[index].material = util.material;
    this.data[index].precio_unitario = util.precio_unitario;
    this.data[index].moneda = util.moneda;
    this.data[index].precio_total = util.precio1 + util.precio2;
    this.guardar();
  };

  eliminarUtil = (id) => {
    this.data.splice(id, 1);
    this.guardar();
  };
}

module.exports = DataUtil;
