const { response , request } = require('express')
const DataUtil = require('../data/data-util')
const uEscolar = require('../models/uEscolares')

const dataUtil = new DataUtil()

const utilGet = (req, res = response) => {

  let datos = dataUtil.getUtiles()

  res.json({
    datos
  })
}


const utilPost = (req = request, res = response) => {
  const { material,precio_unitario, moneda } = req.body;

  let precio1 = precio_unitario['c-1'];
  let precio2 = precio_unitario['c-2'];

  let utilNuevo = new uEscolar(material, precio_unitario, moneda, precio1, precio2)

  dataUtil.addUtil(utilNuevo)

  res.json({
    utilNuevo,
    msg: 'Util Added Sucessfully'
  })
}

const utilPut = (req = request, res = response) => {
  const { id } = req.params
  const { material, precio_unitario,moneda } = req.body;
  let precio1 = precio_unitario['c-1'];
  let precio2 = precio_unitario['c-2'];

  const index = dataUtil.data.findIndex(object => object.id_compra === id)

  if (index !== -1) {
    dataUtil.editarUtil({ material, precio_unitario, moneda, precio1, precio2 }, index)
    res.json({
      msg: 'Util Was Modified Sucessfully'
    })
  } else {
    res.json({
      msg: 'Util Not Found'
    })
  }
}

const utilDelete = (req = request, res = response) => {
  const {id} = req.params
  const index = dataUtil.data.findIndex(object => object.id_compra === id)
  if (index !== -1) {
    dataUtil.eliminarUtil(index)
    res.json({
      msg: 'The Util Was Sucessfully Removed'
    })
  } else {
    res.json({
      msg: 'Util Not Found'
    })
  }
}

module.exports = {
  utilGet,
  utilPost,
  utilPut,
  utilDelete
}