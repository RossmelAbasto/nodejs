const { generarArchivos } = require('./helpers/operacionesYarg')
const { generarResta } = require('./helpers/operacionesYarg')
const { generarMulti } = require('./helpers/operacionesYarg')
const { generarDivision } = require('./helpers/operacionesYarg')

const argv = require('yargs').argv;


generarArchivos(argv.base)
generarResta(argv.base)
generarMulti(argv.base)
generarDivision(argv.base)